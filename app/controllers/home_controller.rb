class HomeController < ApplicationController

  def index
    @title= "MOODSELECTOR"
  end

  # random color theme action
  def random_color_theme
    config = YAML::load_file("config/key.yml")
    kuler = Kuler.new(config["key"])

    if kuler == nil
      puts "kuler IS NILLLLL"
    end

    theme = kuler.fetch_random_theme
    hex_codes = theme.hex_codes

    respond_to do |format|
      format.json { render :json => theme }
    end
  end
end
