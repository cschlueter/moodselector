var custom = {
  paper: undefined,
  flow_paper: undefined,
  current_paper: undefined,
  is_flow_paper: false,
  themes: [],
  current_theme: undefined,
  default_theme: {"theme_id":000000,"title":"nb","tags":["pochette","re","vae"],"rating":5,"img_src":"http://kuler-api.adobe.com/kuler/themeImages/theme_477904.png","author_name":"nb","author_id":000000,"swatches":[{"hex_code":"#99d9f2"},{"hex_code":"#99e2f2"},{"hex_code":"#c9ebf2"},{"hex_code":"#5fcdd9"},{"hex_code":"#f2ebdc"}]},
  direction: "left",
  current_screen_width: 0,
  current_screen_height: 0,
  tiling_methods: ["random", "row", "column", "grid"],
  current_tiling_method: "random",
  current_delay: 100,
  current_tiling_finished: true,

  init: function() {
    // create canvas
    custom.paper = Raphael("color-panel-view", tactile.Device.screen.getWidth(), tactile.Device.screen.getHeight());
    custom.flow_paper = Raphael("flow-panel-view", tactile.Device.screen.getWidth(), tactile.Device.screen.getHeight());
    custom.current_paper = custom.paper;

    // start the game loop, aeeehh color loop 
    custom.nbGetTheme();

    // info view
    var info_view = tactile.page.getComponent("info-view");
    info_view.hide();

    // add button tap listener
    tactile.EventManager.addListener("info", "tap", function(e) {
      // if share view currently visible hide it and show info view
      if (share_view.isVisible()) {
        share_view.hide(); 
      }
      info_view.toggle();
    });

    // share view
    var share_view = tactile.page.getComponent("share-multiview");
    share_view.hide();
    // add button tap listener
    tactile.EventManager.addListener("share", "tap", function(e) {
      // if info view currently visible hide it and show share view
      if (info_view.isVisible()) {
        info_view.hide(); 
      }
      share_view.toggle();
    });

    // add flow tap listener
    tactile.EventManager.addListener("flow", "tap", function(e) {
      // set paper to draw on
      custom.current_paper = custom.flow_paper;
      custom.is_flow_paper = true;

      custom.current_delay = 300;

      var info = tactile.page.getComponent("info");
      var share = tactile.page.getComponent("share");
      var flow = tactile.page.getComponent("flow");
      var title_view = tactile.page.getComponent("title-view");
      var subtitle_view = tactile.page.getComponent("subtitle-view");
      var hint_view = tactile.page.getComponent("hint-view");
      var exit_flow = tactile.page.getComponent("exit-flow");
      info.hide();
      share.hide();
      flow.hide();
      info_view.hide(); 
      share_view.hide();
      title_view.hide();
      subtitle_view.hide();
      hint_view.hide();
      exit_flow.show();
      
      // switch to flow view
      var presenter = tactile.page.getComponent("presenter");
      presenter.showLast();

      custom.flowColorLoop(); 
    });

    // exit flow tap listener
    tactile.EventManager.addListener("exit-flow", "tap", function(e) {
      // set paper to draw on
      custom.current_paper.clear();

      custom.current_delay = 100;
      custom.current_tiling_method = "random";

      custom.current_paper = custom.paper;
      custom.is_flow_paper = false;

      var exit_flow = tactile.page.getComponent("exit-flow");
      exit_flow.hide({to: 0});

      var info = tactile.page.getComponent("info");
      var share = tactile.page.getComponent("share");
      var flow = tactile.page.getComponent("flow");
      var title_view = tactile.page.getComponent("title-view");
      var subtitle_view = tactile.page.getComponent("subtitle-view");
      var hint_view = tactile.page.getComponent("hint-view");
      info.show();
      share.show();
      flow.show();
      title_view.show();
      subtitle_view.show();
      hint_view.show();

      // switch to main view
      var presenter = tactile.page.getComponent("presenter");
      presenter.showFirst();
    });

    // register root layout for swipe event
    tactile.EventManager.addListener("color-panel", "swipe", function(e) {
      custom.direction = e.direction;
      custom.nbPaintColorPanel();
    });

    // register for resize events, but ignore screen width/height by event
    // because we redraw the hole screen anyways
    tactile.EventManager.addResizeListener(function(e) {
      custom.nbPaintColorPanel(e);
    });

    // append image placeholder for info area
    document.getElementById("info-image").innerHTML = "<img id=\"info-img\" src=\"\" />";
    var share_img_tags = document.getElementsByClassName("share-image");
    for (var i=0; i<share_img_tags.length; i++) {
      share_img_tags[i].innerHTML = "<img class=\"share-imgs\" src=\"\" />";
    }

    // insert some icons on the client side into the dom
    // I guess I'm doing something wrong with the ic/IMAGE tag?!
    document.getElementById("fb-icon").innerHTML = "<img style=\"position: relative; left: 250px; top: -62px;\" src=\"assets/Facebook_32.png\" />";
    document.getElementById("twitter-icon").innerHTML = "<img style=\"position: relative; left: 210px; top: -62px;\" src=\"assets/Twitter_32.png\" />";
    document.getElementById("skype-icon").innerHTML = "<img style=\"position: relative; left: 160px; top: -62px;\" src=\"assets/Skypre_32.png\" />";

    // start out with a default theme
    custom.nbPaintColorPanel();
  },

  flowColorLoop: function() {
    if (custom.is_flow_paper === false) {
      custom.current_tiling_finished = true;
      return;
    }
    // only really paint if current tiling finished, timing issues!?
    if (custom.current_tiling_finished === true) {
      custom.current_tiling_method = custom.tiling_methods[Math.floor(Math.random()*4)];
      custom.nbPaintColorPanel();
    }
    setTimeout("custom.flowColorLoop()", 5000);
  },

  nbInspectObject: function(obj) {
    console.log("typeof:" + typeof obj);
    for(var key in obj) {
      console.log("   key:" + key + ", typeof: " + typeof key);
    }
  },

  nbUpdateCurrentTheme: function() {
    if (custom.themes.length > 0) {
      custom.current_theme = custom.themes.pop();
    } else {
      custom.current_theme = custom.default_theme;
    }
  },

  nbUpdatePanels: function() {
    // info panel
    document.getElementById("info-title").textContent = "TITLE: " + custom.current_theme.title;
    document.getElementById("info-author").textContent = "AUTHOR: " + custom.current_theme.author_name;
    var hex_codes = custom.nbExtractHexCodes(custom.current_theme);
    document.getElementById("info-hex").textContent = "HEX VALUES: " + custom.nbGetHexValueString(hex_codes);
    document.getElementById("info-rating").textContent = "RATING: " + custom.current_theme.rating;
    document.getElementById("info-img").setAttribute("src", custom.current_theme.img_src);

    // share panel
    var share_imgs = document.getElementsByClassName("share-imgs");
    for (var i=0; i<share_imgs.length; i++) {
      share_imgs[i].setAttribute("src", custom.current_theme.img_src);
    }
  },

  nbExtractHexCodes: function(theme) {
    // extract hex codes from theme
    var hex_codes = [];
    var hex_code;
    for (var i=0; i<5; i++) {
      hex_code = theme.swatches[i].hex_code;
      hex_codes.push(hex_code);
    }
    return hex_codes;
  },

  nbGetHexValueString: function(color) {
    return color[0] + ", " + color[1] + ", " + color[2] + ", " + color[3] + ", " + color[4];
  },

  // get a fresh theme from the backend color/theme service 
  nbGetTheme: function() {
    // if we already have some colors wait until new once are necessary, we don't wont to upset
    // the webservice with unnecessary requests
    if (custom.themes.length >= 5) {
      setTimeout("custom.nbGetTheme()", 5000);
      return;
    } 

    var sourceloader = new tactile.ContentLoader();

    sourceloader.onsuccess.subscribe(function(e) {
      var theme = JSON.parse(e.loader.getResponseText());
      custom.themes.push(theme);

      // get a new theme until we have five
      setTimeout("custom.nbGetTheme()", 500);
    });
    sourceloader.onerror.subscribe(function(e) {
      console.log("error happened while getting remote colors: " + e.status + ", " + e.message);
      console.log("we just use a default theme for now");

      // get a new theme until we have five
      setTimeout("custom.nbGetTheme()", 5000);
    });
    sourceloader.load({url : "/home/random_color_theme?format=json"});
  },

  nbPaintColorPanel: function() {
    // update current theme/color
    custom.nbUpdateCurrentTheme();
    var hex_codes = custom.nbExtractHexCodes(custom.current_theme);

    // before we paint update the infos
    custom.nbUpdatePanels();

    // tile size
    var box_width = 120;
    var box_height = 120;

    // find out how many tiles have to be drawn
    var xsteps = Math.floor(tactile.Device.screen.getWidth()/box_width);
    var ysteps = Math.floor(tactile.Device.screen.getHeight()/box_height);

    // tiles often can not always fill the hole screen
    // get offsets to center the color grid
    var xmargin = (tactile.Device.screen.getWidth() - (xsteps * box_width)) / 2;
    var ymargin = (tactile.Device.screen.getHeight() - (ysteps * box_height)) / 2;
    
    // clear canvas and resize if necessary
    custom.current_paper.clear();
    custom.current_paper.setSize(tactile.Device.screen.getWidth(), tactile.Device.screen.getHeight());

    if (custom.current_tiling_method === "random") {
      var tiles = custom.randomTiling(xsteps, ysteps, hex_codes, box_width, box_height, xmargin, ymargin);
      custom.paintTile(tiles);
    } else if (custom.current_tiling_method === "row") {
      var tiles = custom.rowTiling(xsteps, ysteps, hex_codes, box_width, box_height, xmargin, ymargin);
      custom.paintTile(tiles);
    } else if (custom.current_tiling_method === "column") {
      var tiles = custom.columnTiling(xsteps, ysteps, hex_codes, box_width, box_height, xmargin, ymargin);
      custom.paintTile(tiles);
    } else if (custom.current_tiling_method === "grid") {
      custom.gridTiling(xsteps, ysteps, hex_codes, box_width, box_height, xmargin, ymargin);
    }
  },
  
  // recursive fun
  paintTile: function(tiles) {
    if (tiles.length <= 0) {
      custom.current_tiling_finished = true;
      return;
    }
    custom.current_tiling_finished = false;
    var tile = tiles.pop();

    custom.current_paper.rect(tile[0], tile[1], 0, 0, 0).attr({stroke: tile[4], fill: tile[4]}).animate({width: tile[2], height: tile[3]}, 500);
    setTimeout(function() { custom.paintTile(tiles, custom.current_delay); }, custom.current_delay);
  },

  gridTiling: function(xsteps, ysteps, hex_codes, box_width, box_height, xmargin, ymargin) {
    // build up the color grid
    var timing = 0;
    for (var i=0; i<xsteps; i++) {
      for (var j=0; j<ysteps; j++) {
        var color = hex_codes[Math.floor(Math.random()*5)].toString();
        if (custom.direction === "left") {
          timeing = ((xsteps-i)*100) + 800;
        } else {
          timeing = (i*100) + 800;
        }
        custom.current_paper.rect(xmargin+((i*box_width)+3), ymargin+((j*box_height)+3), 0, 0, 0).attr({stroke: color, fill: color}).animate({width: box_width-6, height: box_height-6}, timeing);
      }
    }
  },

  randomTiling: function(xsteps, ysteps, hex_codes, box_width, box_height, xmargin, ymargin) {
    var tiles = [];
    for (var i=0; i<xsteps; i++) {
      for (var j=0; j<ysteps; j++) {
        var color = hex_codes[Math.floor(Math.random()*5)].toString();
        tiles.push([
          xmargin+((i*box_width)+3), 
          ymargin+((j*box_height)+3), 
          box_width-6, 
          box_height-6,
          color,
          i + ":" + j
        ]);
      }
    }
    return custom.shuffle(tiles);
  },

  // row tiling
  rowTiling: function(xsteps, ysteps, hex_codes, box_width, box_height, xmargin, ymargin) {
    var tiles = [];
    var is_even = false;
    for (var j=0; j<ysteps; j++) {
      if (is_even === false) {
        for (var i=0; i<xsteps; i++) {
          var color = hex_codes[Math.floor(Math.random()*5)].toString();
          tiles.push([
            xmargin+((i*box_width)+3), 
            ymargin+((j*box_height)+3), 
            box_width-6, 
            box_height-6,
            color,
            i + ":" + j
          ]);
        }
        is_even = true;
      } else {
        for (var i=xsteps-1; i>=0; i--) {
          var color = hex_codes[Math.floor(Math.random()*5)].toString();
          tiles.push([
            xmargin+((i*box_width)+3), 
            ymargin+((j*box_height)+3), 
            box_width-6, 
            box_height-6,
            color,
            i + ":" + j
          ]);
        }
        is_even = false;
      }
    }
    return tiles.reverse();
  },

  // column tiling
  columnTiling: function(xsteps, ysteps, hex_codes, box_width, box_height, xmargin, ymargin) {
    var tiles = [];
    var is_even = false;
    for (var i=0; i<xsteps; i++) {
      if (is_even === false) {
        for (var j=0; j<ysteps; j++) {
          var color = hex_codes[Math.floor(Math.random()*5)].toString();
          tiles.push([
            xmargin+((i*box_width)+3), 
            ymargin+((j*box_height)+3), 
            box_width-6, 
            box_height-6,
            color,
            i + ":" + j
          ]);
        }
        is_even = true;
      } else {
        for (var j=ysteps-1; j>=0; j--) {
          var color = hex_codes[Math.floor(Math.random()*5)].toString();
          tiles.push([
            xmargin+((i*box_width)+3), 
            ymargin+((j*box_height)+3), 
            box_width-6, 
            box_height-6,
            color,
            i + ":" + j
          ]);
        }
        is_even = false;
      }
    }
    return tiles.reverse();
  },

  // linker hand algorithmus und so ;-)
  circleTiling: function(xsteps, ysteps, hex_codes, box_width, box_height, xmargin, ymargin) {
  },

  // http://stackoverflow.com/questions/962802/is-it-correct-to-use-javascript-array-sort-method-for-shuffling
  shuffle: function(array) {
    var tmp, current, top = array.length;

    if(top) while(--top) {
      current = Math.floor(Math.random() * (top + 1));
      tmp = array[current];
      array[current] = array[top];
      array[top] = tmp;
    }
    return array;
  },
};

tactile.page.onready(custom.init);
